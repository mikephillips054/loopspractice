/*
Name: Mike Phillips
Date: 15/03/18  
Student ID: 30000874
*/

let timesNum = 0


timesNum = Number(prompt("Which times table do you want to see?"))

//heading
console.log() 
console.log(`${timesNum} times table:`)
console.log("---------------")
console.log() 

//for Loop
console.log(">>> for loop <<<")
for (i = 1; i <= 12; i++) {
    console.log(`${i} x ${timesNum} = ${i * timesNum}`)
}

console.log()

//while Loop
console.log(">>> while loop <<<")
i = 1
while ( i <= 12) {
    console.log(`${i} x ${timesNum} = ${i * timesNum}`)
    i++
}

console.log()

//do while Loop
console.log(">>> do while loop <<<")
i = 1
do {
    console.log(`${i} x ${timesNum} = ${i * timesNum}`)
    i++
} while ( i <= 12)
