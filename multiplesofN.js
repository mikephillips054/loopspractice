/* 
Name: Mike Phillips
Date: 15/03/18      
Student ID: 30000874
*/ 

let startNum = 3
let endNum = 7

//Heading
console.log("N      10*N      100*N      1000*N")
console.log("-      ----      -----      ------")

console.log() 

//for Loop
console.log(">>> for loop <<<")
for (i = startNum; i <= endNum; i++) {
    console.log(`${i}      ${i * 10}        ${i * 100}        ${i * 1000}`)
}

console.log()

//while Loop
console.log(">>> while loop <<<")
i = startNum
while ( i <= endNum) {
    console.log(`${i}      ${i * 10}        ${i * 100}        ${i * 1000}`)
    i++
}

console.log()

//do while Loop
console.log(">>> do while loop <<<")
i = startNum
do {
    console.log(`${i}      ${i * 10}        ${i * 100}        ${i * 1000}`)
    i++
} while ( i <= endNum) 
