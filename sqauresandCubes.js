/*
Name: Mike Phillips
Date: 15/03/18      
Student ID: 30000874        
*/

console.log() 
console.log("Number    Square     Cube")
console.log("------    ------     ----")
console.log() 



//for Loop
console.log(">>> for loop <<<")
for (x = 0; x <= 10; x++) {
    console.log(`${x}         ${x**2}          ${x**3}`)
}

console.log()

//while Loop
console.log(">>> while loop <<<")
x = 1
while ( x <= 10) {
    console.log(`${x}         ${x**2}          ${x**3}`)
    x++
}

console.log()

//do while Loop
console.log(">>> do while loop <<<")
x = 1
do {
    console.log(`${x}         ${x**2}          ${x**3}`)
    x++
} while ( x <= 10)

console.log();
